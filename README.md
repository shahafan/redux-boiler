#  redux boiler

# Test Tasks

1. The naming of the redux action, reducer, types etc.. are WRONG and need to be fixed, you should refactor the code to match what the api call is doing
2. The homepage should show a list of ALL the entities from the api call and they should be clickable
3. The api call on the redux action is hard coded for a specific entity, after you made a list of entities on the homepage you should make it dynamic from the route.
4. BONUS - Add another middleware that gives an alert if the entity id is 5

Please note:
The design is NOT important for that task.
I want to see proper commits and pr.
Please tell me at the end how much time it took you.

GOOD LUCK

# Get Started
1. `npm install`
2. Launch environment:
  **Production**: `npm start`
  **Development**: `npm run dev`
3. Build for production: `npm run build:prod`
4. Test: `npm test`
