'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanPlugin = require('./utils/clean-plugin');
const NodeUtils = require('./config/node-service');
const appConfig = require('./config/config');

const extractSass = new ExtractTextPlugin({
  filename : `app.css`,
  allChunks: true,
  disable  : process.env.NODE_ENV === 'development'
});

const config = {
  mode: process.env.NODE_ENV,
  devtool: 'cheap-module-source-map',
  output : {
    path      : path.join(__dirname, 'dist'),
    filename  : `app.js`,
    publicPath: '/'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.scss']
  },
  plugins: [
    new CleanPlugin({
      files: ['dist/*']
    }),
    extractSass,
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src/index.html'),
      inject  : 'body'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(
          process.env.NODE_ENV
        ),
        APP_CONFIG: JSON.stringify(
          appConfig
        )
      }
    }),
  ],
  module: {
    exprContextCritical: false, // Suppress "The request of a dependency is an expression"
    rules              : [
      {
        test   : /\.(js|jsx)$/,
        loaders: 'babel-loader',
        exclude: [/node_modules/]
      },
      {
        test: /\.scss$/,
        use : extractSass.extract({
          use: [
            {
              loader : 'css-loader',
              options: {
                modules       : {
                  localIdentName: '[name]__[local]___[hash:base64:5]',
                  exportLocalsConvention: 'camelCase',
                },
                importLoaders : 1,
                sourceMap     : true
              }
            }, {
              loader: 'postcss-loader'
            }, {
              loader : 'sass-loader',
            }
          ],
          fallback: 'style-loader'
        })
      },
      {
        test   : /\.css$/,
        loaders: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test   : /\.(eot|woff|woff2|ttf|png|jpg|svg)$/,
        loader : 'file-loader?limit=10000&name=[name]-[hash].[ext]',
        include: path.join(__dirname, 'src')
      },
      {
        test   : /\.json$/,
        loader : 'json-loader',
        include: path.join(__dirname, 'src')
      },
      {
        test  : /\.svg/,
        loader: 'svg-url-loader'
      }
    ]
  }
};

if (NodeUtils.isProduction()) {
  config.entry = './src/Bootstrap';
} else {
  config.devtool = 'source-map';
  config.entry = [
    'react-hot-loader/patch',
    `webpack-dev-server/client?http://localhost:${appConfig.local.port}`,
    'webpack/hot/only-dev-server',
    './src/Bootstrap'
  ];
  config.plugins.push(
    new webpack.HotModuleReplacementPlugin()
  );
}
/* const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
config.plugins.push(
  new BundleAnalyzerPlugin({analyzerMode: 'static'})
); */

module.exports = config;
