import { combineReducers } from 'redux';
import images from './images/reducer';

export default combineReducers({
  images,
});
