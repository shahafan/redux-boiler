import {
  IMAGES_ADD_IMAGE,
  IMAGES_UPDATE_IMAGE,
  IMAGES_SELECT_IMAGE,
} from './types';

export const imagesAddImage = data => ({
  type: IMAGES_ADD_IMAGE,
  data,
});

export const imagesUpdateImage = data => ({
  type: IMAGES_UPDATE_IMAGE,
  data,
});

export const imagesSelectImage = id => ({
  type: IMAGES_SELECT_IMAGE,
  id,
});
