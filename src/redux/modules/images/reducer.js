import INITIAL_STATE from './initial-state';
import {
  IMAGES_ADD_IMAGE,
  IMAGES_UPDATE_IMAGE,
  IMAGES_SELECT_IMAGE,
} from './types';

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case IMAGES_ADD_IMAGE: {
      return {
        ...state,
        data: [
          ...state.data, ...[action.data],
        ],
      };
    }
    case IMAGES_UPDATE_IMAGE: {
      return {
        ...state,
        data: state.data.map(image => (image.id === action.data.id
          // transform the one with a matching id
          ? { ...image, ...action.data }
          // otherwise return original
          : image)),
      };
    }
    case IMAGES_SELECT_IMAGE: {
      return {
        ...state,
        data: state.data.map(image => ({ ...image, selected: image.id === action.id })),
      };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
