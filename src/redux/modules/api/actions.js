import HttpRequest from '../../../libs/HttpRequest';

export const httpRequestAction = (action, dispatch) => {
  const {
    url,
    method = 'get',
    data = {},
    params = {},
    success,
    failure,
    loader,
    debug = false,
  } = action.payload;

  debug && console.log(action.payload);

  const { base, endpoint } = url;

  !loader || dispatch(loader(true));

  const options = {
    method,
    url: base ? base + endpoint : endpoint,
  };
  if (Object.keys(data).length) { options.data = data; }
  if (Object.keys(params).length) { options.params = params; }
  debug && console.log(options);
  return HttpRequest()(options)
    .then(response =>
      // debug && console.log(response);
      dispatch(success(response.data)))
    .then(() => !loader || dispatch(loader(false)))
    .catch((e) => {
      debug && console.log(e);
      return dispatch(failure(e.message || ''));
    });
};

export const api = action => (dispatch, getState) => {
  return dispatch(action);
};
