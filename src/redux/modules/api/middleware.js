import { API } from './types';
import { httpRequestAction } from './actions';

const api = ({ dispatch, getState }) => next => (action) => {
  if (action.type !== API) {
    return next(action);
  }
  httpRequestAction(action, dispatch);
};

export default api;
