import config from '../../../common/config';
import {
  INSTANCES_FETCH_DATA,
  INSTANCES_FETCH_CURRENT,
  INSTANCES_IS_LOADING,
  INSTANCES_HAS_ERROR,
} from './types';
import { API } from '../api/types';
import { api } from '../api/actions';

const ENDPOINT = 'instances';

const API_PREFIX = `/${ENDPOINT}`;

export const instancesFetchDataSuccess = data => ({
  type: INSTANCES_FETCH_DATA,
  data,
});

export const instancesFetchCurrentSuccess = data => ({
  type: INSTANCES_FETCH_CURRENT,
  data,
});

export const instancesIsLoading = bool => ({
  type     : INSTANCES_IS_LOADING,
  isLoading: bool,
});

export const instancesHasError = bool => ({
  type    : INSTANCES_HAS_ERROR,
  hasError: bool,
});

export const instancesDoNothing = data => ({
  type: 'NOTHING',
});

export const instancesFetchData = () => (dispatch, getState) => dispatch(api({
  type   : API,
  payload: {
    url: {
      base    : config.api.url,
      endpoint: API_PREFIX,
    },
    params: {
      ...getState().general.routeParams,
    },
    method : 'get',
    success: data => instancesFetchDataSuccess(data),
    failure: bool => instancesHasError(bool),
    loader : bool => instancesIsLoading(bool),
  },
}));

export const instancesFetchCurrent = id => (dispatch, getState) => dispatch(api({
  type   : API,
  payload: {
    url: {
      base    : config.api.url,
      endpoint: `${API_PREFIX}/${id}`,
    },
    params: {
      ...getState().general.routeParams,
    },
    method : 'get',
    success: data => instancesFetchCurrentSuccess(data),
    failure: bool => instancesHasError(bool),
    loader : bool => instancesIsLoading(bool),
  },
}));
