import INITIAL_STATE from './initial-state';
import {
  INSTANCES_FETCH_DATA,
  INSTANCES_FETCH_CURRENT,
  INSTANCES_IS_LOADING,
  INSTANCES_HAS_ERROR,
} from './types';

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INSTANCES_FETCH_DATA: {
      return { ...state, data: action.data };
    }
    case INSTANCES_FETCH_CURRENT: {
      return { ...state, current: action.data };
    }
    case INSTANCES_IS_LOADING: {
      return { ...state, isLoading: action.isLoading };
    }
    case INSTANCES_HAS_ERROR: {
      return { ...state, hasError: action.hasError };
    }
    default: {
      return state;
    }
  }
};

export default reducer;
