import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { logger } from 'redux-logger';
import rootReducer from '../modules';
import api from '../modules/api/middleware';

/**
 * Determine which Redux store to provide based on the
 * Environment Type of Node.js
 * @return {object}    Redux store
 */

const middlewares = [
  thunk,
  api
];

if (process.env.NODE_ENV !== 'production' && process.browser) {
  middlewares.push(logger);
}

// noinspection JSCheckFunctionSignatures
const store = (initialState = {}) => (
  createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middlewares)
  )
);

export default store;
