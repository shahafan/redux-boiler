import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.scss';

class App extends React.Component {
  render () {
    return (
        <div className={styles.testApp}>Home</div>
    );
  }
}

App.propTypes = {
  
};

const mapStateToProps = (state) => {
  return {
    
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
