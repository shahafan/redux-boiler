import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import configureStore from './redux/store/configure-store';
import Root from './Root';

// Require globals
import 'babel-polyfill';

const store = configureStore();


const Tiles = {
  init: ({ el, checkout }) => {

    ReactDOM.render(
      <AppContainer>
        <Root store={store} />
      </AppContainer>,
      document.getElementById(el)
    );

    // Hot Module Replacement API
    if (module.hot) {
      module.hot.accept('./Root', () => {
        const NextApp = require('./Root').default;
        ReactDOM.render(
          <AppContainer>
            <NextApp store={store} />
          </AppContainer>,
          document.getElementById(el)
        );
      });
    }
  }
};

window.Tiles = Tiles;
0

Tiles.init({
  el: 'root',
  images: [
    {
      "name": "1596719682.jpg",
      "size": "1253883",
      "width": 3840,
      "height": 2560,
      "src": "http://localhost/squarez/uploads/tails_files/19536/1596719682.jpg"
    },
    {
      "name": "1596719682_3.jpg",
      "size": "2152474",
      "width": 3840,
      "height": 2560,
      "src": "http://localhost/squarez/uploads/tails_files/19536/1596719682_3.jpg"
    },
    {
      "name": "1596719682_2.jpg",
      "size": "1848125",
      "width": 3840,
      "height": 2560,
      "src": "http://localhost/squarez/uploads/tails_files/19536/1596719682_2.jpg"
    }
  ]
});
